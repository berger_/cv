
# Job Search

**_CV, Bewerbungsschreiben & Arbeitszeugnisse_**

I want to save everything related to Job Search in this repo.

## Structure of this Repo

Currently (August 2024) I created a new CV in Typst and in German, as I currently need it in German for my job search in Südtirol.

This is found in `german-typst`.

The old CV was written in Latex and in English and is found in `english-latex`.
Note that I think I'm not able to really compile this anymore. See the `README` there for more information.

**The most current CV is right now the German CV.**


## Typst vs. Latex

I tried Typst now, and I have to say I like it a lot!

In my opinion, I should stay with it, and if I need an English CV again, I should rewrite that in Typst.