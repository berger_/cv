#import "moderncv/moderncv.typ": project, cvcol, cventry, cvstudy
// #import "moderncv/moderncv_nogrid.typ": project, cvcol, cventry, cvstudy

#set text(font: "Fira Sans")

#show: project.with(
  title: "Softwareentwickler",
  // title: "Software Engineer",
  author: "Dominik Berger",
  // github: "johndoe1337",
  phone: "+39 351 616 2949",
  // email: "dominik.berger@pm.me",
  email: "dominik.berger@mailbox.org",
  address: "Pichl 19, 39050 Jenesien",
  // address: "Vittorio-Veneto-Straße 3, 39100 Bozen",
  birthday: "18. Jänner 1996",
  profile_picture: "img/compressed_compressed_cropped.jpg",
  

  main_color: rgb("#36594F"),
  heading_color: rgb("#36594F"),
  // job_color: ...
)

= Arbeitserfahrung

#cventry(
  start: (month: "März", year: "2023"),
  end: (month: "Juni", year: "2024"),
  role: [Softwareentwickler (Teilzeit 20h)],
  location: "München",
  place: "TUM School of Engineering and Design / Lehrstuhl für Architekturinformatik")[
    // Entwicklung mehrer Prototypen anhand des Münchner Digitalen Zwillings in Flutter (Dart), Unity (C\#) und Firebase.
    // In Zusammenarbeit mit Studenten und Mitarbeitern des Lehrstuhls wurde ein Geo-Tool entwickelt, das die Arbeit mit Geodaten vereinfacht.
    // Weiters erstellte ich mehrere interaktive 3D-Welten in three.js und Unity und half den Studenten darauf aufzubauen.
    
    #v(0.5em)
    Entwicklung mehrerer Prototypen anhand des Münchner Digitalen Zwillings unter Verwendung u.a. von Flutter (Dart), Unity (C\#) und Google Firebase.
    - Entwicklung eines Geo-Tools, das die Arbeit mit Geodaten für Studenten und Mitarbeitern des Lehrstuhls Geodaten vereinfacht
    - Erstellung interaktiver 3D-Welten in `three.js` und Unity, welche als Grundlage für weitere Projekte der Studenten dienten
    - Implementierung einer mobilen App in Flutter für iOS und Android mit 3D-Komponenten in `three.js`
    - Aufbau eines Backends mit Google Firebase zur Unterstützung der Projekte
    - Unterstützung der Studenten bei der Arbeit mit Unity und Flutter sowie bei der Nutzung von Geodaten-Tools wie PostGIS (PostgreSQL) oder QGIS anhand von Workshops und individueller Betreuung


//      * TODO: Was noch?
//      - Arbeiten / managen mit anderen Personen?
//      - Rust
//      - Geodaten (PostGIS, QGIS, GDAL)
//      - three.js, Blender
//         - Sonst noch Zeug aus dem Arbeitszeugnis von Gerhard? Was hat er so geschrieben?

// Notizen aus Obsidian:
// - Eine mobile App in Flutter für iOS und Android mit 3D-Komponenten in three.js
// - Ein Backend mit Google Firebase erstellt
// - Grundlagen für einen Digital Twin des Münchner Viertels Neuperlach in Unity erstellt
// - Prototyp eines Geo-Tools zur Vereinfachung der Arbeit mit den Daten über Tools wie PostGIS, QGIS, Rust
// - Unterstützung der Studierenden in Unity und Flutter


]

#cventry(
  start: (month: "Jänner", year: "2021"),
  end: (month: "Dezember", year: "2021"),
  location: "Remote",
  role: [Softwareentwickler (Freelance, Vollzeit)],
  place: "Payback")[
    Vollzeit bis Oktober 2021, dann Teilzeit bis Ende 2021.
    Als Freelance-Softwareentwickler unterstützte ich die Firma in verschiedenen Projekten:
    // TODO: Kann ich hier schon REWE sagen?
    - Implementierung von OpenID Connect und dadurch Ermöglichung des Logins der Partner-Kunden (z.B. REWE) über den Payback-Login
    - Design und Implementierung eines neuen firmeninternen E2E-Test-Frameworks basierend auf Java Spring, das Testzeiten reduzierte und die Benutzung vereinfachte
    // TODO: Should I say "Elasticsearch / Logstash / Kibana"?
    // TODO: Vllt besser: Verbesserung der Automation?
    - Vereinfachung und Automatisierung des ELK-Monitoring Stacks anhand Ansible und Jenkins
    - Migration der firmeninternen Backoffice Applikation von Java EE zu Spring Boot, wobei Startup- und Compilation-Zeit stark verbessert wurden und die Applikation vorbereitet wurde für eine Microservice-Architektur


// As a freelance software engineer in Java, I
// supported the company for one year in the following topics:
// Implement OpenID Connect and therefore allow customers of the client’s website to log in using their
// Payback credentials, which was greatly anticipated by some of the biggest clients;
// ○ Design and implement a company-internal E2E testing framework based on Java Spring, which reduced
// execution time heavily;
// ○ Migrate the ELK monitoring stack to the newest version while keeping the monitoring operational;
// rework its Jenkins and Ansible pipeline on the way, which made the system less complex and improved
// its maintainability;
// ○ Migrate the company’s back office system from Java EE to Spring Boot, therefore improving its startup
// and compilation time, and making it ready for a microservice architecture.
    
]

#pagebreak()

#cventry(
  start: (month: "Dezember", year: "2018"),
  end: (month: "Mai", year: "2020"),
  location: "München",
  role: [Softwareentwickler (Werkstudent)],
  place: "Payback")[
    // TODO: Maybe remove some things here? For example the E2E / async/await?
    Als Werkstudent arbeitete ich mit Angular, TypeScript, Protractor, Java EE und dem ELK Stack vor allem an der
    firmeninternen Backoffice-Applikation.
    - Umstellung einer Java EE-Anwendung von SOAP auf REST und Anpassung des Angular-Frontends
    // - Erweiterung der End-to-End-Protractortests mit async/await von JavaScript 8, um deren Zuverlässigkeit zu erhöhen
    - Realisierung von Echtzeit-Updates durch Implementierung von Websockets in Java EE und Angular
    - Optimierung der Continuous Delivery-Pipeline des Elasticsearch-Stacks, inklusive Docker-gestützter Integrationstests

// As a student software developer, I was working with Angular, TypeScript, Protractor, Java EE, as well as the
// Elasticsearch stack.
// - Refactor a Java EE application from SOAP to REST architecture and adapt the Angular frontend;
// - Extend the end-to-end Protractor tests with the new async/await feature of JavaScript 8, therefore making
// them more reliable;
// - Implement websockets in Java EE and Angular to allow updates in real-time;
// - Improve the Continuous Delivery pipeline of the existing Elasticsearch stack, including docker-based
// integration tests.
    
]



#cventry(
  start: (month: "Oktober", year: "2017"),
  end: (month: "April", year: "2018"),
  location: "München",
  role: [Tutor],
  place: "TUM")[
    Tutor für den Einführungskurs in die Informatik.
// Tutorial Assistant for the intro course to computer science.
    
]


#cventry(
  start: (month: "Juli", year: "2014"),
  end: (month: "August", year: "2014"),
  location: "Bozen",
  role: [Softwareentwickler (Praktikum)],
  place: "HGV / Booking Südtirol")[
    Entwicklung eines Monitoring-Services für das Buchungsportal in Java.
// Develop a small monitoring service for the booking portal in Java.    
]


#cventry(
  start: (month: "Juni", year: "2014"),
  end: (month: "Juni", year: "2014"),
  location: "Bozen",
  role: [Softwareentwickler (Praktikum)],
  place: "Astat")[
    Umsetzung einer Website zur interaktiven Datenanzeige mit Google Web Toolkit und MySQL.
// Develop a website to interactively display data with GWT and MySQL.
]





// #cvcol[
//   ==== Generic stuff
// 
//   My other stuff goes here
// ]


= Ausbildung

#cvstudy(
  start: (month: "Oktober", year: "2020"),
  end: (month: "September", year: "2024"),
  // title: [Master of Science in Informatik],
  title: [M.Sc. Informatik],
  place: "Technische Universität München (TUM)")[
    Mit zwischenzeitiger Unterbrechung im Jahr 2021.
    
    *Zwischennote*: 1,6

    *Masterarbeit*: _Automatically Detecting Flakiness-Introducing Commits in Continuous Integration_.
    Anhand verschiedener Daten aus Code, Git und CI konnte ich automatisch Flakiness-einführende Commits identifizieren.
    Das Verfahren testete ich am Chromium-Code und dessen CI.
    // In German: Automatische Identifizierung Flakiness einführender Commits in Continuous Integration
  ]


#cvstudy(
  start: (month: "Oktober", year: "2016"),
  end: (month: "September", year: "2020"),
  title: [B.Sc. Informatik],
  place: "Technische Universität München (TUM)")[
    //// I currently did not add this, since I have the Master's degree now and that thesis is more interesting.

    // *Bachelorarbeit*: Berechnung von oberen Schranken für PlanlängenBerechnung einer oberen Schranke bei Planungsproblemen mit einem SAT-basierten Ansatz, der zur Verbesserung der Suche nach einer Lösung für ein gegebenes Planungsproblem verwendet werden kann.
    
    // *Publikation*: Abdulaziz, M. und Berger, D. Berechnung von Planlängen-Schranken mithilfe der Längen der längsten Pfade. Proceedings der AAAI Conference on Artificial Intelligence. 35, 13 (Mai 2021), 11709-11717.
]

#cvstudy(
  start: (month: "Oktober", year: "2015"),
  end: (month: "September", year: "2016"),
  title: [Maschinenbau-Studium],
  place: "Technische Universität München (TUM)")[
    Ein Jahr Bachelor-Studium Maschinenbau, danach Wechsel zu Informatik.
  ]

#cvstudy(
  start: (month: "September", year: "2010"),
  end: (month: "Juli", year: "2015"),
  title: [Oberschule mit Schwerpunkt Informatik],
  place: "Technologische Fachoberschule Bozen (TFO)")[]



#pagebreak()

= Technische Skills
// Oder: Programmiersprachen & Frameworks (?)

#cvcol[
  ==== Programmiersprachen, Libraries & Frameworks

  #grid(
    columns: (1fr, 1fr, 1.1fr),
    row-gutter: 0.5em,
    [- Java],
    [- Java EE],
    [- Spring Boot],
    [- Python],
    [- Django],
    [- Pandas / Polars],
    [- JavaScript],
    [- TypeScript],
    [- HTML5/CSS3],
    [- Angular],
    [- SQL], // TODO: Or write PostgreSQL / MySQL / SQLite
    [- PostGIS (PostgreSQL)],
    [- Flutter],
    [- C\#],
    [- Unity],
    [- REST],
    [- SOAP],
    // maybe also:
    // more likely: MongoDB, React, C, C++, Unity, Svelte
    // less likely: Clojure, OCaml, SML
  )
]

#cvcol[
  ==== Tools

  #grid(
    columns: (1fr, 1fr, 1fr),
    row-gutter: 0.5em,
    [- Git],
    // [- SVN],
    [- Docker],
    [- Ansible],
    [- Jenkins],
    [- GitHub Actions],
    [- ELK / Elasticsearch],
  )
]




= Sprachkenntnisse



#cvcol[
  #grid(
    columns: (.9fr, 1.1fr),
    row-gutter: 0.5em,
    [
      - *Deutsch*: Muttersprache
      - *Italienisch*: Gut
    ],
    [
       // TOEFL: 107/120 (Reading: 28/30, Listening: 29/30, Speaking: 22/30, Writing: 28/30)
      - *Englisch*: Sehr gut (TOEFL: 107/120)
      - *Französisch*: Anfänger
    ],
  )
]


= Ehrenamtliche Arbeit

#cventry(
  start: (month: "", year: "2015"),
  end: (month: "", year: "2018"),
  role: [Mitglied, Netzwerkadministrator],
  place: "Akaflieg München")[
    Ein studentischer Verein, in welchen ich am Bau und der Konstruktion von Segelflugzeugen von Grund auf bis zur Flugzulassung beteiligt war.
    Neben dem Erlernen der Konstruktion eines Segelfliegers im Team pflegte ich die IT des Vereins und erneuerte unter anderem den Mailserver.
]




// = Soft Skills und Teamwork

// TODO: Should I add this?
// TODO: Gerade bei ersteren sollte ich etwas sagen können auch dazu - was haben wir denn da gemacht?

// - Two semester-long course on soft skills with a practical part, which involved topics such as communication,
// teamwork, and time management.
// - Experience with working in a Scrum team, producing highly-valued deliverables every other week

//// Translated in German:

// - Zwei Semester umfassender Kurs zu Soft Skills mit praktischem Anteil, der Themen wie Kommunikation, Teamarbeit und Zeitmanagement beinhaltete
// - Erfahrung in der Arbeit mit Scrum, um im zweiwöchigen Rhythmus hochwertige Deliverables zu produzieren