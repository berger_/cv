// This is originally copied from modernc.typ.
// It exists solely for pandoc to create a (mostly) nicely formatted Markdown document out of the CV.
// That is useful for spellchecking and for ChatGPT.

#let left_column_size = 25%
#let grid_column_gutter = 8pt

#let main_color = rgb(147, 14, 14)
#let heading_color = main_color
#let job_color = rgb("#737373")


#let nametitle(author, title) = [
  // Author information.
  #text([#author], weight: 400, 2.5em)
    
  #v(-1.2em)
    
  // Title row.
  #block(text(weight: 400, 1.5em, title, style: "italic", fill: job_color))
]


#let authorinfo(
  alignment: right + top, github, address, phone, email, birthday, extra_info
) = [
  #align(alignment)[
      // Contact information
      #set block(below: 0.5em)

      #if github != "" {
        align(top)[
          #box(height: 1em, baseline: 20%)[#pad(right: 0.4em)[#image("icons/github.svg")]]
          #link("https://github.com/" + github)[#github]
        ]
      }

      #if address != "" {
        align(top)[
          #box(height: 1em, baseline: 20%)[#pad(right: 0.4em)[#image("icons/home-outline.svg")]]
          #text(address)
        ]
      }

      #if phone != "" {
        align(top)[
          #box(height: 1em, baseline: 20%)[#pad(right: 0.4em)[#image("icons/phone-outline.svg")]]
          #link("tel:" + phone)[#phone]
        ]
      }

      #if email != "" {
        align(top)[
          #box(height: 1em, baseline: 20%)[#pad(right: 0.4em)[#image("icons/envelope-outline.svg")]]
          #link("mailto:" + email)
        ]
      }

      #if birthday != "" {
        align(top)[
          #box(height: 1em, baseline: 20%)[#pad(right: 0.4em)[#image("icons/birthday-cake-outline.svg")]]
          #text(birthday)
        ]
      }

      #if extra_info != "" {
        align(top)[
          #block(extra_info)
        ]
      }
    ]
]


#let project(
  title: "",
  author: [],
  phone: "",
  address: "",
  birthday: "",
  email: "",
  github: "",
  extra_info: "",
  profile_picture: "",
  left_column_size: left_column_size,
  grid_column_gutter: grid_column_gutter,
  main_color: main_color,
  heading_color: heading_color,
  job_color: job_color,
  body
) = {
  set document(author: author, title: title)
  set page(numbering: none)
  set text(font: "Fira Sans", lang: "en", fallback: true)
  show math.equation: set text(weight: 400)

  /*
   * How headings are used:
   * - h1: section (colored, prominent, with colored rectangle, spans two columns)
   * - h2: role (bold)
   * - h3: place (italic)
   * - h4: generic heading (normal, colored)
   */
  show heading.where(level: 1): element => [
    #v(0em)
    #box(
      inset: (right: grid_column_gutter, bottom: 0.1em),
      rect(fill: main_color, width: left_column_size, height: 0.25em)
    )
    #text(element.body, fill: heading_color, weight: 400)
    #v(.5em)
  ]

  show heading.where(level: 2): element => [
    #text(element.body + ",", size: 0.8em)
  ]

  show heading.where(level: 3): element => [
    #text(element.body, size: 1em, weight: 400, style: "italic")
  ]

  show heading.where(level: 4): element => block[
    #text(element.body, size: 1em, weight: 400, fill: heading_color)
    #v(.2em)
  ]

  set list(marker: box(circle(radius: 0.2em, stroke: heading_color), inset: (top: 0.15em)))

  set enum(numbering: (n) => text(fill: heading_color, [#n.]))

  if profile_picture == "" {
    grid(
      columns: (1fr, 1fr),
      box[
        #nametitle(author, title)
      ],
      authorinfo(
        alignment: right + top,
        github, address, phone, email, birthday, extra_info
      )
    )
  } else {
    grid(
      columns: (1fr, 1fr),
      box[
        #nametitle(author, title)
        #v(1em)
        #authorinfo(
          alignment: left,
          github, address, phone, email, birthday, extra_info
        )
      ],
      align(
        right,
        [        
          #v(.3em)
          #rect(image("../" + profile_picture, height: 11em), radius: 1%, stroke: luma(20))
        ]
      )
    )
  }
  v(.8em)

  // Main body.
  set par(justify: true, leading: 0.5em)

  body
}

#let datebox(month: "", year: []) = box(
  align(center,
    stack(
      dir: ttb,
      spacing: 0.4em,
      text(size: 1em, [#year]),
      if month != "" [
        #text(size: 0.75em, month)
      ],
    )
  )
)

#let daterange(start: (month: "", year: []), end: (month: "", year: [])) = box(
  grid(
    columns: (4em, .8em, 4em),
    gutter: .5em,
    [#datebox(month: start.month, year: start.year)],
    [--],
    [#datebox(month: end.month, year: end.year)]
  )
)

#let cvgrid(..cells) = pad(bottom: 0.8em)[#grid(
  columns: (left_column_size, auto),
  row-gutter: 0em,
  column-gutter: grid_column_gutter,
  ..cells
)]

#let cvcol(content) = cvgrid([], content)

#let xdot(s) = {
  if s.ends-with(".") {
    s
  } else {
    s + "."
  }
}

#let cventry(
  description,
  start: (month: "", year: ""),
  end: (month: "", year: ""),
  place: "",
  role: []
) = [
  == #place
  === #role
  #v(-.2em)
  #start.month #start.year -- #end.month #end.year
  #v(-.2em)
  #description
  #v(.5em)
]
// ) = cvgrid(
//   align(center, daterange(start: start, end: end)),
//   [
//     == #place
//     === #role
//     #v(.5em)
//   ],
//   [],
//   description
// )

// Same as `cventry`, places the role / title first.
#let cvstudy(
  description,
  start: (month: "", year: ""),
  end: (month: "", year: ""),
  place: "",
  title: []
) = [
  == #title
  === #place
  #v(-.2em)
  #start.month #start.year -- #end.month #end.year
  #v(-.2em)
  #description
  #v(.5em)
]
// ) = cvgrid(
//   align(center, daterange(start: start, end: end)),
//   [
//     == #title
//     === #place
//     #v(.5em)
//   ],
//   [],
//   description
// )

#let cvlanguage(
  language: [],
  description: [],
  certificate: [],
) = cvgrid(
  align(right, language),
  [#description #h(3em) #text(style: "italic", certificate)],
)
