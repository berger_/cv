
# German CV (in Typst)

I used `typst 0.11.1 (50115102)` (from `typst --version`) to generate this CV.

## The Template

This is the original template that I used here: https://github.com/giovanniberti/moderncv.typst.

However, I changed it a little.

## Alternative Templates I Liked

I had some others that I liked.
I will just put them here for the future.

- https://github.com/caffeinatedgaze/bare-bones-cv
- https://github.com/mizlan/typst-resume-sans
- https://github.com/elegaanz/vercanard

And here is a list of many CVs in Typst: https://github.com/qjcg/awesome-typst?tab=readme-ov-file#cv.