Sehr geehrte Damen und Herren,

durch die Empfehlung von Manuel Pillon (im Team von Klemens Rauch) wurde ich auf die offene Stelle als Full-Stack-Developer in Ihrem Unternehmen aufmerksam, auf welche ich mich hiermit mit großem Interesse bewerbe. Als Softwareentwickler mit umfassender Erfahrung in komplexen Backend-Systemen und Webanwendungen bin ich überzeugt, dass ich einen wertvollen Beitrag zu Ihrem Team leisten kann. Ich verfüge über fundierte Kenntnisse in den geforderten Technologien C#, SQL/PostgreSQL, HTML5/CSS3 und JavaScript. Auch bin ich sicher, dass ich durch mein Wissen in Angular schnell in Vue.js/Nuxt.js einsteigen kann.

Momentan stehe ich kurz vor dem Abschluss meines Master-Studiums in Informatik an der Technischen Universität München (TUM), wobei ich vorher und währenddessen bereits reichlich Erfahrungen in der Softwareentwicklung sammeln konnte und somit ein breites Spektrum an aktuellen Fähigkeiten mitbringe.

Während meines Master-Studiums arbeitete ich in Teilzeit am Lehrstuhl für Architekturinformatik an der TUM, wo ich nicht nur innovative Prototypen in Flutter, JavaScript und Unity entwickelte, sondern auch intensiv mit einem interdisziplinären Team zusammenarbeitete. Ich unterstützte meine Kollegen bei der Nutzung verschiedenster Technologien z.T. mit selbst erstellten Tools und Libraries und leitete u.a. einen Workshop bei dem die Teilnehmer lernten, geobasierte 3D-Anwendungen zusammen mit PostGIS (PostgreSQL) zu entwickeln.

Vor meiner Tätigkeit am Lehrstuhl war ich bei Payback als Softwareentwickler tätig, wo ich in einem agilen Scrum-Team unter anderem an der Implementierung von OpenID Connect (zur Bereitstellung des Payback-Logins an die Partner-Firmen) und der Entwicklung eines E2E-Testframeworks beteiligt war, wobei Java (Spring Boot und Java EE), Angular, TypeScript/JavaScript, sowie Python als Technologien zum Einsatz kamen. Diese Erfahrungen haben meine Fähigkeit gestärkt, effiziente und skalierbare Lösungen zu entwickeln.

Zuletzt habe ich im Rahmen meiner Masterarbeit ein Verfahren entwickelt, welches anhand CI automatisch Commits erkennt, welche Flakiness in Tests einführen. Ich fand das Thema äußerst spannend und konnte meine Kenntnisse in Softwareentwicklung, Testautomatisierung und Data Analysis weiter vertiefen.

Ich bin überzeugt, dass meine technische Expertise und meine Leidenschaft für innovative Lösungen einen wertvollen Beitrag zu Ihrem Team leisten können. Meinen Lebenslauf sowie mein letztes Arbeitszeugnis habe ich beigefügt. Ich würde mich freuen, Sie in einem persönlichen Gespräch von meinen Fähigkeiten zu überzeugen und mehr über die Position zu erfahren.

Ich freue mich auf Ihre Rückmeldung.


Mit freundlichen Grüßen,

Dominik Berger