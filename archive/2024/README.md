
# Bewerbungsphase September/Oktober 2024

See below for the job offers I had and why I chose Alpitronic.

I still want to do some things to wrap this Bewerbungsphase up:

- [x] Note down the salary negotiation sites
- [x] Add my own experience regarding that (though that is to write that it mostly fits!)
- [ ] Store some notes on Additive, Reheg (that I have on paper currently)
- [ ] Maybe store the emails, especially Alpitronic, as they show a job offer negotiation
- [ ] Store the Additive insight day challenges / assignments


## The 3 Job Offers I had in the end

In the end (at the beginning of October) I had three job offers:

- Alpitronic

  - First: 50k base, 20% bonus, 33 vacation days
  - I sent them (via mail) a counter-offer of 57k base, all else equal
  - I got a new offer: 52k base, 20% bonus
  - ✅ I accepted this offer

- Reheg

  - They really are just 2 people, but they could pay me really well!
  - The reason I declined this in the end were a few:
    - Mainly HO (which I really like a lot, though not at this point in my life, where I would like to work with a team more)
    - They are just starting out, and I want to learn/work with a bigger team at this point
    - Though they look really competent, at this point they don't really have a finished product yet. However, they kinda
      do though! ASA Pay is just going to start, which is developed by them.
  - What they offered, which I quite liked, was 36h work week, which really is nice.
  - They were just all around nice people - again, I was quite sad to choose Alpitronic over them.
  - Also, they said **maybe another time!** So if I'm still interested in the future, I could still contact them again!
  - They offered 48k for the first 6 months, then 55k (all for 36h work week, 30 vacation days (1 day = 8h))
  - ❌ In the end I declined, for the reasons stated above

- Additive

  - They had the best financial offer, but I didn't like their work culture
    - I also am not a big fan of PHP - I should stop applying at PHP shops tbh, it's not worth it
    - I can get around PLs like C# etc., but not PHP, I think
    - [ ] I have some screenshots and pictures from their actual offer - I still want to copy these into this repo
  - Their initial offer was something between 45k-55k excl. bonus (which is 10k); I knew that because we talked about money very early already
    - Because I knew that, I declined the offer before getting a direct number
    - At this point he said he'd have gone with something like 53k (though I doubt that that is actually true)
    - Since I said that is all too low (even 55k), he first said, OK, they cannot offer that and that was that (that was on a Friday)
    - On Monday, he called again with an offer of 56k (though he said that is the highest they could go)
      - This would be 66k incl. the whole bonus!
    - Side note: I did not like that he said it was over, but then called again. When he called again he turned it a little
      as if it was somehow my fault that he had to do it like this (i.e. now call again).. of course he could've just said
      on Friday that he needs to check again
  - ❌ In the end, I declined the offer though it was the financially best offer. I just didn't like their work culture
    - Also their product isn't directly the most ethical one (online marketing). That combined with their work culture doesn't give good vibes to me.


- Alpin is still outstanding, and though the people seemed really nice, I don't think the work is too interesting