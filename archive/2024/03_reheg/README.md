
reheg: https://www.reheg.com/#welcome
https://www.suedtirolerjobs.it/jobs/software-entwickler-in-9,106422

Beworben am: 03.09.2024

---

Vorstellungsgespräch 06.09.2024:

Ich habe geredet mit:
- Thomas Niedermair (CEO und Hauptentwickler)
- Lukas Achammer (macht glaube ich wirtschaftliche Sachen)

Es sind momentan nur die 2 in der Firma.

Gehalt:
- Ich habe gesagt ich hätte gerne 65k brutto im Jahr
- Sie haben das mal notiert. Ich glaube sie wollen schon noch verhandeln.
- Sie haben 2 Sachen gesagt:
    - Es wäre ja auch nur 36h Woche (Vertrag machen sie für 36h nur! Was ja cool ist)
        - **Freitag nachmittag frei!**
    - Und sie würden vllt erstmal weniger und dann nach spätestens 6 Monaten aufstocken.
- Worüber wir nicht geredet haben ist die Abfertigung. Da muss ich mal schauen wie das ist.

Vielleicht ein anderes Mal zu fragen:
- Sie haben iwie gemeint sie machen dann schon 40h
    - Aber ich glaube da haben sie nur sich selbst gemeint? Iwie leicht unklar
    - Ich sollte in jeden Fall bzgl **Überstunden** fragen
    - Auch zu fragen: **Urlaubstage**
- Was ich auch noch fragen könnte/sollte, ist **Software-Konferenzen** / **Weiterbildung**
    - Das wäre ja auch ein Benefit für mich
- Vllt noch konkreter wegen Home-Office fragen
    - Auch: Ist es OK, mal 2-3 Wochen durchweg HO zu machen?
- Sie schreiben eigtl immer alles als Microservices hat Thomas gemeint
    - Wieso? Was sind die Vorteile? Ich glaube das wäre auch interessant zu fragen

Was ansonsten ganz cool war:
- Die zwei waren mir sehr sympathisch
- Beide studiert, finde ich gut
- Flexible Arbeitszeiten
- Home-Office passt auch


Sie wollen sich bis in so **1,5 Wochen** melden.
Also denk mal so bis 18.09.2024 oder so.


**Update 19.09.24**:

Thomas hat vorhin nochmal angerufen.
Eigentlich wollen sie mich immer noch, aber 65k ist zu viel.

- Eher 42k
- Ich meinte: 50k ist meine Schmerzgrenze
- Es wäre ja **58,5k bei 36h**
    - Aber eben **50k ist Schmerzgrenze**
    - (Ich glaube 50k hier wäre relativ gut: sind ja auch **36h!!!** D.h. sonst wären das 56,5k!)
- Ich habe dann auch gesagt, evtl muss man noch Urlaubstage oder so reden
- Und natürlich mit den ersten 6 Monaten (also danach evtl mehr)

Weiters kurz geredet:
- Konferenzen gehen sie eigtl nicht, aber evtl könnte man (sie haben nicht ganz verstand was ich meine, glaube ich)
- Sie wollen generell schon noch wachsen. Er hofft, dass das neue Produkt das dann auch hergibt.
    - Gerade das finde ich eigentlich auch sehr cool :)

Also **kurz**:
- Wenn wir auf 50k bei 36h kommen, klingt eigentlich alles ganz cool
- Dann ist nur zu schauen bei den anderen Unternehmen noch, ob mich nicht Alpitronic zB mehr interessiert


---

Anruf am 20.09.24:

Bzgl Gehalt sind sie nochmal runtergegangen:

- Ich hätte 6 Wochen Urlaub (laut Tarif ist wohl nur 5 Wochen)
- Jetzt: 42/44k danach nach 6 Monate recht fix 50k
  - Zuerst hat er 42k gesagt, danach dann dass sie auch auf 44k hochkönnten
- Ich habe gesagt ich muss nochmal darüber denken. Da ist er dann eben gleich auf 44k hoch (für die 6 Monate)
  - Ich hab dann weiter gesagt, dass ich gerne noch nächste Woche abwarten möchte
- Das hat soweit gepasst und wir sehen und hören uns Anfang Oktober wieder

- Es ginge auch bei der ASA zu arbeiten
- Weil unter denen haben sie sich kurzgeschlossen: Deshalb kommt auch ASA weniger in Frage, weil ich da eher bei der
  REHEG arbeiten sollte

Ich glaube ein kleines bisschen ginge es noch zu verhandeln. ;)

Wir hören/sehen uns nochmal in Kaltern Anfang Oktober, zusammen mit ASA.

Bis dahin zu überlegen:

- Lieber REHEG oder Alpitronic? (oder sonst was, z.B. Alpin?)
- Falls REHEG: Will ich nochmal hochhandeln?
