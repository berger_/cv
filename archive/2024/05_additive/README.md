
Beworben am: 05.09.2024

---

Heute (10.09.) Anruf von Stefan Moser

- Sie benutzen Laravel
- Sprache: Deutsch, unter den Entwicklern Englisch
  - Englisch weil man wohl mit anderen in remote arbeitet... tun sie vielleicht Sachen outsourcen?
    - Das würde eher nicht so gut klingen
- Es klingt allgemein nicht so gut
- Studium war zB nicht so wichtig hatte es den Anschein
- Er ließ mich aber auch nicht gscheid erklären, dass ich schon Arbeitserfahrung habe
- Habe das dann schon doch erzählt und wir sind uns einig dass ich ca. Mid-Level bin
- Ich wäre aber dann Backend-Entwickler mit PHP, also das was ich mich beworben habe
- Was er noch gesagt hat (wenn ich mich recht erinnere): sie sind ca 70 insgesamt, davon 17 Entwickler

Gehalt ist am schlimmsten:
- Er meinte, wenn ich es jetzt recht im Kopf habe, Mid-Level (sie nennen das "Expert") wäre bei 35-40k.
- Ich wollte 65k
- Er meinte dann, vllt ginge etwas zwischen 45-50k
- Darauf meinte ich, 50k ist absolute Schmerzgrenze, eigentlich aber schon 55k
- Er meinte, 55k wird wohl nicht gehen.
- Aber so sind wir vorerst verblieben

Dann habe ich jetzt am **23.09.** um 14:00 Uhr ein Vorstellungsgespräch
- In Bozen (s. auch Mail)
- Wichtig: **Mail ist: dmnkbrgr@gmail.com**
  - Habe ich wohl "falsch" angegeben
- Ich soll vorher noch **Laravel lernen** etwas (s. wieder Mail: Da ist basically das Installationstutorial verlinkt ^^)

Was noch gut war:
- Man kann wohl im Ausland arbeiten, zB in Barcelona, Stockholm oder Berlin
  - Da haben sie wohl Appartments und man kann da drin wohnen? Gratis? Das habe ich nicht soo ganz verstanden
  - Aber es geht eben iwie

Was nicht soo gut war:
- Sie wollen, dass man vorher sich schon einlernt
- V.a. halt in Laravel... würde ich eigentlich eh tun glaube ich, von dem her nicht soo schlimm wieder

Noch zu fragen:
- HomeOffice-Möglichkeit?
- Zeiten zu arbeiten? (flexibel?)
- Es wird wohl Scrum gemacht. Wie wird so entwickelt? Entwicklungsplattform? Code Reviews? Tests?

---

Update 23.09.24:

Heute Vorstellungsgespräch gehabt.

Notizen sind im Ordner in Sprachform, die habe ich mir so nachher gemacht.

Kurz zusammengefasst:
- Software ist doch etwas cooler als gedacht, und Head of Software (Peter Putzer) war mir sehr sympathisch
- Ich habe nächste Woche (Anfang Oktober) nochmal einen Inside Day, also einen ganzen Tag dort... habe aber immer noch nicht gehört wann
- Dort werden wir auch nochmal über Gehalt reden. Da sollte ich mir auch nochmal PHP / Laravel vorher anschauen

Mein Eindruck bisher:
- Gehalt ist nochmal zu reden
- PHP/Laravel ist ein bisschen der Nachteil bisher
- Stefan Moser / HR macht mir den Eindruck als ob er mich schlecht reden will (um das Gehalt zu drücken?)
  - Mir kommt ein bisschen vor, dass deswegen + wegen PHP sollte ich das Gehalt nochmal nach oben handeln :)
  - Also doch mehr als 50k verlangen
- Ich sollte dann auch noch wegen Urlaubstage und HO-Policy fragen
