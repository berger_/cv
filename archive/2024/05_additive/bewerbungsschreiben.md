Sehr geehrte Damen und Herren,

mit großem Interesse bewerbe ich mich auf die Position als Backend Developer.
Als Softwareentwickler mit umfassender Erfahrung bin ich überzeugt, dass ich einen wertvollen Beitrag zu Ihrem Team leisten kann, nicht zuletzt, da ich auch in und um der Hotellerie bereits Erfahrungen sammeln konnte, beispielsweise bei einem Praktikum beim HGV für Booking Südtirol.

Momentan stehe ich kurz vor dem Abschluss meines Master-Studiums in Informatik an der Technischen Universität München, wobei ich vorher und währenddessen bereits reichlich Erfahrungen in der Softwareentwicklung sammeln konnte und somit ein breites Spektrum an aktuellen Fähigkeiten mitbringe. Meine Erfahrung umfasst die Entwicklung von Webanwendungen, mobilen Apps und komplexen Backend-Systemen. Ich verfüge über fundierte Kenntnisse in den Technologien SQL/PostgreSQL, REST/JSON/XML sowie Git. Auch bin ich sicher, dass ich durch mein vielseitiges Fachwissen im Backend schnell in PHP einsteigen kann.

Während meines Master-Studiums arbeitete ich in Teilzeit am Lehrstuhl für Architekturinformatik an der TU München (TUM), wo ich nicht nur innovative Prototypen in Flutter, JavaScript und Unity entwickelte, sondern auch intensiv mit einem interdisziplinären Team zusammenarbeitete. Ich unterstützte meine Kollegen bei der Nutzung verschiedenster Technologien z.T. mit selbst erstellten Tools und Libraries und leitete u.a. einen Workshop bei dem die Teilnehmer lernten, geobasierte 3D-Anwendungen zu entwickeln.

Vor meiner Tätigkeit am Lehrstuhl war ich bei Payback als Softwareentwickler tätig, wo ich in einem agilen Scrum-Team unter anderem an der Implementierung von OpenID Connect (zur Bereitstellung des Payback-Logins an die Partner-Firmen) und der Entwicklung eines E2E-Testframeworks beteiligt war, wobei Java (Spring Boot und Java EE), Angular, TypeScript, sowie Python als Technologien zum Einsatz kamen. Diese Erfahrungen haben meine Fähigkeit gestärkt, effiziente und skalierbare Lösungen zu entwickeln.

Zuletzt habe ich im Rahmen meiner Masterarbeit ein Verfahren zur automatischen Erkennung von problematischen Commits in CI-Pipelines entwickelt, um Commits zu erkennen die Flakiness in Tests einführen. Ich fand das Thema äußerst spannend und konnte meine Kenntnisse in Softwareentwicklung und Testautomatisierung weiter vertiefen.

Ich bin überzeugt, dass meine technische Expertise und meine Leidenschaft für innovative Lösungen einen wertvollen Beitrag zu Ihrem Team leisten können. Meinen Lebenslauf habe ich beigefügt. Ich würde mich freuen, Sie in einem persönlichen Gespräch von meinen Fähigkeiten zu überzeugen und mehr über die Position zu erfahren.

Ich freue mich auf Ihre Rückmeldung.


Mit freundlichen Grüßen,

Dominik Berger