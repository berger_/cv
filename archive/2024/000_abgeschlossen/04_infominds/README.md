
Die gehören glaube ich zu ACS. ACS selbst hat auch offene Stellen, aber die habe nicht so gut gepasst.

https://acsgroup.onboard.org/jobs/XmanDj4W?from_career_page=true

Beworben am: 03.09.2024

---

Update 10.09.24:

- Vorstellungsgespräch am Mittwoch, 18.09. um 10:00 auf MS Teams
- Auf italienisch vermutlich
- Wichtig: Wohl doch als **Mobile Entwickler**
  - Das hat die HR-Frau so gesagt, dass das wohl besser für mich passen würde
  - Ich sollte also das nochmal anschauen vielleicht für die Firma


Update 17.09.24:

Ich habe jetzt das Gespräch abgesagt und meine Bewerbung zurückgezogen.

Hatte verschiedene Gründe:
- Bin gerade nicht motiviert auf Bewerbung, weil ich einfach erst gestern MA fertig gemacht habe
- Zudem ist die etwas anstrengender nochmal:
  - Mobile Entwickler (wieso? Backend würde besser passen!)
  - auf italienisch
- Weiters: Die ist auch ACS, so wie consisto.
  - Bei consisto habe ich einen recht guten Eindruck gemacht. Mich würde interessieren, was er da sagen würde bei mir noch. (Auch wenn das Gehalt trotzdem zu wenig ist! Nur 50k wenn überhaupt...)
  - Aber wenn ich hier in italienisch geredet hätte, hätte ich wahrscheinlich einen nicht so guten Eindruck gemacht.
  - Wenn die dann gemeinsam reden, ist das blöd für mich.
