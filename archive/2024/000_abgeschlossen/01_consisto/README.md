
Beworben am 28.08.2024 über mailbox.org.

https://www.consisto.it/de/jobs.html

---

Personen:
- Alexander Pescollderung (Chef)
- Matthias (IT Chef)


Update 12.09.: Heute Vorstellungsgespräch gehabt onsite.

Zur Firma / genereller:
- Ich habe mich sehr gut verstanden mit Chef (Alexander Pescollderungg)
  - Habe auch mit Matthias gesprochen, dem IT Teamlead (?). Recht sympathisch, hat aber nicht super viel gesprochen
- Mo+Fr HomeOffice, Di-Do onsite
- Fr. geht's meistens früher zu Ende (um 15:00 Uhr). Sind aber schon 40h
  - Wsl also wird die Zeit sonst gemacht? Das habe ich nicht genauer gefragt
- Sie sind 24 Leute, davon 8 Entwickler
- Sie sind erstranging Web-Agentur + bieten Software an Unternehmen an. Größter Abnehmer ist glaube ich hogast
  - Wobei sie erst letztens einige Kunden abweisen mussten, weil sie wohl nicht die Manpower dafür hatten
- Man kann bei ihnen in der Tiefgarage gratis parken
- Vereinzelt geht es auch mit länger HomeOffice, zB mal 2 Wochen am Stück oder so was
- Einer ist auch in 100% HO. Der ist im Vinschgau - sie probieren das wohl gerade, wie gut das geht
  - Das ist aber momentan die einzige Ausnahme und ich glaube er wollte das nicht unbedingt haben
  - Könnte aber natürlich sein, dass dadurch vllt HO wieder mehr kommt... 
  - Ansonsten ist nämlich das Mo/Fr recht streng kommt mir vor
- Das Team ist recht jung. Eigtl alle unter 50 kam mir vor. Vllt einige da herum, aber viele um 20-35.

Programmiertechnisch:
- Sie machen alles auf Windows
- Heißt v.a. C# und .NET
- Darauf aufbauend haben sie auch ihr **eigenes CMS** gemacht, das sie immer wieder benutzen wohl
  - Ich weiß ehrlich gesagt nicht ganz, was ich davon halten soll... klingt iwie etwas over-engineered
- Eigtl arbeitet jeder mehr am eigenen Süppchen. Heißt es gibt ein Projekt und das macht dann eine Person
- Austausch intern vllt eher weniger zwischen den Programmierern. 
- Sie wollen bis Ende des Jahres Code Reviews einführen
- Tests habe ich dann nicht gefragt (wenn dann machen das eh eher nur einzelne)
- Ein anderes Projekt, das sie noch genannt haben ist eine PWA für den Kaminkehrerverband

Gehaltstechnisch
- Ich habe 63k brutto im Jahr gefragt
- Das war zu viel. Er meinte eher bei 30-35k.
- Wir haben uns vorerst auf ca. 50k geeinigt, wobei er da ein Bonussystem einbauen würde und damit könnte ich dann dahin
  kommen
- Also Basis wäre weniger als 50k - mit Bonus dann 50k
- Ich sollte das dann nochmal hochhandeln auf 55k mMn... mal schauen ob das geht
  - Ich habe auch gesagt "50k absolute Schmerzgrenze, eigtl eher Grenze bei 55k"
- Was ich mich auch gerade frage:
  - Es wäre erstes Jahr ein befristeter Vertrag, dann unbefristet
  - _Gibt es bei befristete Verträge eine Abfertigung?_ (das wären ja dann auch nochmal 2500€ oder so die wegfallen würden das erste Jahr)


Was noch:
- Er hat fast 2x (1x fix, ein weiteres mal so halb) davon gesprochen, dass er Angst hat, die Gehälter irgendwann nicht mehr
  bezahlen zu können
  - Er hat damit halt begründet, wieso er weniger zahlen will
  - Ich weiß nicht ob das so ein gutes Zeichen ist
- Was anderes, was mir aufgefallen ist: der CEO ist erst seit 2 Jahren bei consisto
  - Das ist eher negativ?


Wie sind wir verblieben:
- Er muss mit ACS absprechen wegen Gehalt (50k / Bonus, s. oben)
- Er meldet sich. Es kann ein bisschen dauern meinte er. Würde also vmtl mal 2/3/4 Wochen auch warten
- Ich habe, btw, gesagt ich möchte 1. November anfangen


Update 18.09.24:

Heute hat Alexander kurz angerufen und wollte fragen wie es bei mir noch aussieht. Ich habe gesagt bei mir ist schon noch alles offen.
Er hat nochmal gesagt, dass momentan etwas die Projekte fehlen.
Zudem meinte er, als Projektleiter/Vertriebler müsste ich auch vllt 4x die Woche ins Büro, ob das dann Sinn macht.
Weiters kann er mir nicht wirklich eine Karriere bieten, was er auch schade findet.

Ich habe gesagt:
- 4x die Woche ins büro ist zwar OK - aber das Vertrieb/Projektleitung ist das Problem.
- **Ich möchte zuerst noch Backend-/Full-Stack-Entwicklung machen um da einfach noch Erfahrung zu sammeln!**
- **Und das noch gerne für ein paar Jahre**

Also so habe ich ihm das gesagt... er meinte, er versteht, sie haben nur leider momentan nicht den Bedarf dafür.

**Er fragt dann jetzt am WE nochmal herum in der Firma, vllt gibt es ja noch was zu tun für mich. Fand ich recht nett :)**
Und nächste Woche im Laufe der Woche meldet er sich nochmal.


---

Anruf 26.09.:

- Absage sozusagen: Sie haben zu wenig zu tun
- Er behält sich aber meinen CV und schreibt mir evtl. falls es mal was zu tun gibt
- Vom Chef her fand ich ihn auch sehr sympathis :) Also Firma kann ich mir da auch mal merken :)

---

P.S.:
Zu Vertrieb ist vllt. auch das hier interessant: (ich habe nur gerade "informatiker vertrieb" gegoogelt):
- https://www.weiterbildungsmarkt.net/magazin/itler-im-b2b-vertrieb/
- Anderer (mehr geläufigerer Name) ist: **Sales Engineer**

Some HN threads on sales engineers:
- Pretty interesting: https://news.ycombinator.com/item?id=24271256
- Didn't read so much yet:
  - https://news.ycombinator.com/item?id=9025205
  - https://news.ycombinator.com/item?id=35587930

So ganz leicht kommt mir vor ist der Konsens zu eher nicht / eher doch engineer.
Sind natürlich vmtl auch viel Amis, wo SE besser ist als hier in Suedtirol.
