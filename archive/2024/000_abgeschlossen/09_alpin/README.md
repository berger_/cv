
https://jobs.alpin.it/p/e1e0e98be9f401-software-entwickler-software-engineer-auch-part-time-all-genders

Beworben mit ihrer Online-Form (_nicht_ über LinkedIn) am: 19.09.2024

Musste dort alles nochmal eingeben, uff. Naja, ging mit Pandoc verhältnismäßig schnell.

---

Update 24.10.2024:

Das erste Vorstellungsgespräch lief eigentlich ganz gut. Sie meinten sie melden sich in ca. 1-1,5 Wochen.

Leider haben sie mehr als 2 Wochen gebraucht um sich zu melden wieder, bzw. ich habe mich dann zuerst bei ihnen gemeldet, weil ich dann der Alpitronic zugesagt habe.

Ich habe auch nicht wirklich auf sie gewartet (auch wenn es mich interessiert hätte zu wissen, ob sie mich wollen).

Das positivste an der Firma war, dass die 2 Leute (ich glaube 2 der 3 Chefs) sehr sympathisch waren.
Von den Themen war die Firma nicht ganz meines. Ich hätte bei ihnen im eCommerce-Bereich gearbeitet, wo sie u.a. Sportler und Dr. Schär betreuen.

Das wäre auch interessant - aber das war es auch schon ein bisschen.

Mich haben dann noch ein paar Sachen etwas abgeschreckt wie dass sie nicht unbedingt neue Sprachen-Features benutzen wollen (Darüber haben wir zwar nicht weiter geredet, aber sie verwenden Java, und eigentlich finde ich da so gut wie alles neue gut und cool) oder dass sie Eclipse benutzen.

Sie waren auch nur ca. 5 Entwickler - der Rest konfiguriert eine gewisse Dokumentenmanagementsoftware, was aber weniger mit programmieren zu tun hat. (Sie haben eben diese 2 Bereiche: eCommerce + Document Management)

Deshalb habe ich mich dann entschieden, da abzusagen, auch wenn ich noch nix gehört habe.

In der Email hat Christoph Moar (einer der beiden) u.a. das hier geschrieben:

> Wünsche beste Erfolge in deinem anstehenden Wechsel, und würde mich freuen wenn wir uns irgend einmal wieder über den Weg laufen - Südtirol ist ja klein.

Dem stimme ich zu - wie gesagt, wir hatten echt ein nettes Gespräch.

Weiters bedeutet das auch, dass ich mich nochmal bewerben könnte evtl, denke ich :-)

Allerdings bin ich mir nicht ganz sicher, ob ich das selbst möchte. Zum Beispiel würde ich da erstmal eher NOI probieren, von jetzt den Firmen wo ich es probiert habe - aber vielleicht ändert sich ja was in den nächsten Jahren? :-)

