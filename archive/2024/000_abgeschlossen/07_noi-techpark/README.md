
https://www.suedtirolerjobs.it/jobs/software-developer-m-w-d-tech-transfer-digital,107210

Beworben am: 18.09.24

---

Update 24.10.2024:

Ursprünglich haben sie recht bald geantwortet, dass sie eigentlich schon einige Bewerber hatten und deshalb erstmal warten würden. (Das fand ich auch ganz OK, dass sie mir das transparent mitteilen)

Sie haben mich da auch auf den Laufenden gehalten - eigentlich hatten sie einen gefunden.

Als ihnen der vor einer Woche ca. abgesagt hat, haben sie mir dann nochmal geschrieben, dass sie mich nun gerne zu einem Vorstellungsgespräch einladen würden.
Das war kurz nachdem ich bei Alpitronic zugesagt habe.
Also habe ich ebendies geantwortet und dabei ist es nun verblieben.

Die Antwort war:

> alles klar, danke für die Rückmeldung und viel Erfolg in der neuen Anstellung. 
> 
> Vielleicht ergibt sich ja in Zukunft eine Möglichkeit der Zusammenarbeit. 

D.h. wenn ich auch in Zukunft da nochmal interessiert wäre, sollte ich mich nochmal bewerben! :-)

Ich glaube an sich klingt der Job dort ja sehr interessant!

