Dear Sir or Madam,

I recently finished my Bachelor's degree in Computer Science from Technical University Munich (TUM) in Munich, Germany and I am now searching for work in or around the city of Copenhagen as a software engineer.

In this search, I have found your highly interesting job opportunity as a Kotlin developer for Shape. I had a look at your website to see what kind of products your firm is developing, and they look quite awesome. Besides the great functionality they deliver I especially like the design. Even though I have so far not programmed in Kotlin, I am very interested in it, since I have only heard great things of it. Furthermore I am always striving for a new challenge and love learning new things.

I have started my programming path in high school where I already learned Java, C, SQL, as well as some Java EE. After that and some internships in which I got to deepen my Java knowledge, I started university in Munich. In my third semester I tutored at our university's Introduction to Programming course, where Java was taught to the new students. Personally, I learned Python and used that language for some smaller side projects. After my tutoring job, I started as a working student for Payback, a company that develops the biggest bonus programme in Germany.

At Payback I worked on an internal web application which was used for the management of the partners of Payback. The frontend of the application was written in TypeScript together with the Angular framework, the backend using Java EE. I worked on both ends, first RESTifying the application, then implementing WebSockets to notify about changes made by others in real time, and finally also improving the end-to-end Protractor tests. For the last six months of my work there I switched teams and subsequently worked on improving the Elasticsearch (ELK) stack that was used by the whole company to search through the produced logs. During my whole stay at Payback I learned thoroughly about what it means to write clean code as well as how to practice test-driven development.

A few years ago, I got more into functional programming. I wrote the code for my bachelor's thesis in SML (which is similar to OCaml or F#) and I am currently creating a full stack implementation of a books library in Clojure and ClojureScript in my free time, during which I learned lots of new language paradigms and overall improved my knowledge of functional program design, which is definitely useful not only for functional
programs, but also for programming in languages like Java or Kotlin. I also deepened my understanding of several useful
tools such as Docker and using CI pipelines.

All told, I always like to advance my skills and I love new challenges, and I think your job would offer me a new one. I am looking forward to hearing from you.

Sincerely,

Dominik Berger