
# README

Update 2023-08-07:

I **could not get the CV to work locally**. On **Overleaf** it compiles fine!

## What I Tried:

On Manjaro, I installed the following

- `texlive-latexextra`
- `texlive-xetex`
- `texlive-fontsextra`
- `texlive-fontsrecommended`
- `texlive-fontutils`
- `texlive-luatex`

I also tried to install things with `tlmgr`.

Even without `tlmgr`, I could get it to work *almost*.
However, in the end, the fonts were still missing.
That is, all text was displayed as a normal font - nothing was bold or italic.

As said above, on Overleaf, everything worked fine out of the box.
I could not get it to work offline.
