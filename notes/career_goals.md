
# Career Goals

_Created on October 2024._

It probably makes sense to make up some career goals for myself.

This article by Julia Evans has some guidance: https://jvns.ca/blog/2018/09/30/some-possible-career-goals/.

## **_My_** Career Goals

Looking at these, it's really hard to decide _which_ of these apply to me.

Mostly because about 80% of them kind of apply to me!?

Also, it includes _big_ and _small_ goals - for this document here, however, I'm mostly just interested in _big goals_.

Let's see if I can find **3 big goals** that really talk to me:

- Give a talk at a conference or meetup (incl. South Tyrol Tech Talks - size of it doesn't matter)
- Release a software product on my own, whatever its size
  - Could also be a software library
- Start (really) freelancing on my own
  - "Really", compared to what I did before at Payback, which was more luck


Probably later: 
- Become an engineering manager, after first gaining more experience

