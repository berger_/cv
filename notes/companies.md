
# Interesting Companies

I want to make a list of companies that I find interesting.

Side note: These should really be _that_, i.e. companies that I really find interesting.
Of course, when I search for companies I'll probably find new ones. I should note these down in the next job search folder, as they are only relevant there. _If_ I see that they are interesting, but I don't start a job there (yet?), I can write them down here!

Before I do that here, however, I want to note some other documents where I store things like that:

- Obsidian, in my Personal Vault (here: https://gitlab.com/berger_/personal-obsidian; see for example **Firmen Südtirol**)
- On my iPhone notes

I think I'd like to put them all here at some point, but I just want to note to keep these in mind also.

## Companies List

- [Teamscale](https://teamscale.com/)

I know them from my master's thesis, though I didn't directly work with them. However, my adisor (Fabian Leinen) did!
They look quite interesting! They are fully remote in Germany, and do interesting things in Software. 

The company behind them is actually CQSE, I think.

- [Dynatrace](https://careers.dynatrace.com/locations/innsbruck/)

They are located (also?) in Innsbruck. I just found that interesting, which is why I'm saving this here.
I already heard of them, though I'm not sure where exactly. However, I know that they are a real software company, and I find that quite interesting.

## Companies in South Tyrol (Südtirol)

Copied verbatim from my Personal Obsidian, where I now removed it.

Another note: These are also _slightly_ outdated, since for example I already applied to ASA. However, it is still interesting for the future of course!

Siehe auch das Projekt, wo ich suedtirolerjobs.it scrape

- https://www.kreatif.it/
- https://www.keepinmind.info/

- ASA Hotel sollte auch interessant sein -> die machen halt Software in erster Linie!
    * Notiz: Ich habe sie jetzt (Oktober 2024) ja schon angeschrieben und diese Arbeitssuche wird das nix mehr. Aber interessant: Sie wollen ziemlich wachsen und sind jetzt dabei ein größeres Büro zu bauen.

Bei meiner Arbeitssuche im September/Oktober 2024 hatte ich noch einige aufgeschrieben, die ich eigentlich noch anschreiben wollte, wo ich aber nicht dazu gekommen bin. Ob sie wirklich gut sind oder nicht, kann ich so nicht sagen - ich wollte sie trotzdem einfach mal kurz aufschreiben:

- ELCA
- Progress
- Fraunhofer Italia
- https://www.smart-dato.com/en/
    - A guy from there does a talk in this meetup: https://www.meetup.com/tech-talks-south-tyrol/events/305718747/?eventOrigin=group_upcoming_events

### Online-Job-Portale

- suedtirolerjobs.it
- karriere-suedtirol.com
- joobz.it (scheint aber nicht so aktiv)

### Arbeitsagenturen

Moni meinte, man könnte/sollte auch Arbeitsagenturen probieren. Also dass ich denen meinen CV schicke und sie dann für mich Arbeitgeber suchen.
Vielleicht findet man dadurch ja auch weitere interessante!

Er hat gemeint, er hat mit Businesspool gute Erfahrung gemacht: https://www.businesspool.eu/de/
Sollte ich auf jeden Fall mal probieren!

## Companies from the old list

This list is from the old repo `job-applications` which I'm now porting over to here.

Especially the first job (Innomedic) could be really interesting!


- Innomedic

Empfohlen von Inge (Mama von Kathi). Die suchen gute Informatiker. Dort hat Inge auch für mich Infos eingeholt bzgl Freelancer. Ich glaube sie hat da auch selbst gearbeitet.

Falls Interesse, müsste ich aber auch sie dazu fragen nochmal!

Potentiell gut als Freelancer!

www.innomedic.de


- Imandra

www.imandra.ai

I just found it on Github, since a Clojure core contributor works there. Then I saw that the company has a seat in Edinburgh as well. Obviously I don't know where the Clj contributor works, but I found the company interesting.
Note from October 2024: I think I actually added this back when I was a little bit looking for companies in Edinburgh, as I thought
that Kathi might go there.
