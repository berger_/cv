
# Gehalt

Es gibt gar einige gute Websites mit Infos über Gehalt und vor allem **Gehaltsverhandlungen**.

Diese hier ist ein guter Start: https://www.kalzumeus.com/2012/01/23/salary-negotiation/.

Die hat mir Stuppner gegeben im September 2024. Leider war ich da bei eigentlich allen Firmen wo ich mich beworben habe
schon etwas weiter und habe wohl den größten Fehler gemacht: eine Zahl gleich zu nennen.

Ich denke/hoffe, dass ich eh eine relativ hohe Zahl gesetzt habe.
**Allerdings glaube ich, dass die Strategie, gar keine Zahl zu nennen, wirklich besser ist!**

Auf jeden Fall würde es sich lohnen, die Strategie mal zu probieren!

Das einzige, was ich eben dachte, ist, dass es sich in Südtirol lohnen könnte gleich schon eine hohe Zahl zu nennen um
die Verhandlungsbasis höher zu setzen.
Ich glaube der Punkt dieser Blog Posts ist aber, **dass man das immer noch tun kann - sobald die andere Seite halt mal
eine Zahl gesagt hat!**

Eine weitere Seite, die ich gut fand ist: https://fearlesssalarynegotiation.com/salary-negotiation-guide/.
Die hat auch weitere Artikel die gut sind, z.B.: https://fearlesssalarynegotiation.com/salary-negotiation-email-sample/.

Dann gibt es noch weitere, die ich gerade offen hatte. Die habe ich nicht gelesen, könnten aber auch gut sein:
- https://haseebq.com/how-not-to-bomb-your-offer-negotiation/
- https://bayareabelletrist.medium.com/how-i-negotiated-a-software-engineer-offer-in-silicon-valley-f11590f5c656


## Meine Erfahrung

Ich kann jetzt gerade nur über Südtirol schreiben, aber im Grunde treffen die Sachen aus den Blog Posts für Südtirol schon
auch zu!

Einige Punkte:

- Wenn man mit einer No-Fucks-Given Haltung reingeht, kriegt man mehr. Man muss dafür bereit sein, wegzugehen.
  - Deshalb vermute ich auch, dass man hätte demerst mehr rausholen können, also wenn ich keine Zahl gesagt hätte.
- Man kann immer ein Gegenangebot machen. 10%-20% sind da wirklich angebracht, auch wenn es bei mir nur auf 4% rauslief leider bei Alpitronic.
  - Was Alpitronic angeht, da gibt es verschiedenes zu bemerken:
    - Ich war im Vorstellungsgespräch nicht super stark (wie das bei anderen Firmen der Fall war). Mein größter Vorteil war hier eindeutig, dass ich Pillon kenne.
    - Ich wollte dorthin, deshalb war ich auch nicht so bereit, mega viel zu handeln.
    - Es war auch gegen Ende der restlichen Verhandlungen - ich war müde und wollte einen Job.
    - Ich hätte aber nochmal hingehen können und hätte wahrscheinlich auch mehr rausholen können. Ich glaube, vom Geld wäre etwas mehr drin gewesen (keine Ahnung aber ob wirklich bzw. wieviel), aber sicher wäre sonst noch Sachen drin gewesen, z.B. Urlaub, HO, oder Weiterbildung.
    - Randnotiz: Hoffentlich ist HO + Weiterbildung jetzt trotzdem drin... wie gesagt, ich war es irgendwann müde zu verhandeln.
- Per Email geht auch und ist vielleicht wirklich leichter/angenehmer als am Telefon.
- Ich glaube aber auch, dass ich nicht _so_ schlecht bin beim Reden. Trotzdem verhasple ich mich immer wieder mal, oder sage dann auch eine Zahl die tiefer ist als ich ursprünglich wollte. Deshalb würde ich sagen, ist Email wirklich besser.


## Zusammenfassung der Blog Posts usw.

Momentan habe ich nur den Engineering Kiosk.

- [ ] Die anderen Blog Posts würde ich hier auch noch gerne zusammenfassen.

### Podcast: Engineering Kiosk

Hier ist ein Podcast über Jobsuche: https://engineeringkiosk.dev/podcast/episode/119-der-jobwechsel-einblick-und-erfahrungsaustausch-mit-unmute-it/.

Der war recht gut/interessant. **Also auch allgemein gut und interessant - nicht nur bzgl Gehalt!**

Bei 01:50:06 sagt Wolfi:

> Darum ist es, glaube ich, wichtig, keine Zahl zu nennen, möglichst lange zumindest. Das machen ja die meisten in dem Interviewprozess sehr früh und die wollen da auch eine Zahl, aber eigentlich kann man das normal immer rauszögern oder eine Range geben oder eine sehr hohe Range, einfach so ganz grob, damit es nicht ganz off ist. Aber ich würde mir auf keine Zahl möglichst lange festlegen.

Ich finde die Idee gut.

Momentan (September 2024) suche ich in Südtirol und habe bisher immer wieder so 65k brutto gesagt (also eine Zahl gleich zu Beginn).
Mittlerweile denke ich mir, ich sollte eher nix sagen bzw. wenn sie fragen dann sagen 50-70k.
Das will ich auch die nächsten Male probieren jetzt.

Das hat 2 Vorteile:
- Sehr offen, d.h. sie wissen schon ich will noch eher in die obere Range dort
- Untere Grenze ist gesetzt: 50k
  - Das ist vor allem gut, weil gar einige ziemlich low-ballen wollen kommt mir vor und dann mit Sachen zwischen 30-45k kommen
  - Und das kommt für mich halt gar nicht in Frage...

Gleich nachher auch noch interessante Passagen zu Gehalt:

> Beziehungsweise es gibt ja auch die Möglichkeit, selber zu fragen, was ist denn eure Range von der Jobposition? Also gar nicht selber irgendeine Zahl sagen, sondern das frühzeitig fragen, weil dann sitzt du am längeren Hebel und dann einfach sagen, macht mir ein faires Angebot und dann geht's weiter.

Darauf:

> Nicht nur total sinnvoll, meines Erachtens nach Pflicht, weil der Punkt ist einfach, wenn man nach der Gehaltsrange fragt und dann nichts bekommt, kann man nicht wissen, wo man mit seinem Gehalt ist und ob die Stelle überhaupt für relevant ist, weil du investierst so viel Zeit während des Interviewsprozesses. Das ist eine meiner ersten Fragen, wie sieht die Gehaltsrange aus? Und dann wird mir oft irgendwas gesagt, dann kann ich sagen, okay, ich verdiene jetzt gerade aktuell mehr oder was weiß der Geier nicht. Weil verschlechtern möchte man sich ja auch nicht.

> Viele Firmen haben natürlich keine Range, das ist ein Problem, aber dann kannst du ja fragen, okay, was verdient bei euch irgendwie ein Senior Developer? Ich weiß nicht, vielleicht bin ich kein Senior, aber nur mal zur Orientierung, was verdient ein Senior bei euch oder so irgendwas? Also, dass man in irgendeine Zahl kommt.
