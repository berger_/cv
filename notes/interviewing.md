
# Some Tips on Interviewing

Here is an article by Julia Evans: [Questions I'm asking in interviews](https://jvns.ca/blog/2013/12/30/questions-im-asking-in-interviews/)

This is the main part:

> My basic strategy is to spend 20 minutes before each interview I do and pick some appropriate questions from this list.
> 
> [...]
> 
> - I don’t ask all of these in first interviews. Use your discretion, and do what you feel comfortable doing.
> - Asking a lot of questions shows that you value yourself and that you’re careful when making decisions. It’s a good thing.
> - If you have an offer, you can schedule extra conversations if you feel that not all of your questions have been answered.
> - It can be worth asking the same question to more than one person.

Then, there is the list of questions. **They are all so good!**

I really like that there are questions for companies that look very professional already (i.e. questions that are more proficient) and then others that are fairly basic (issue tracker, roadmap, etc.).
Then, there are also other kinds of questions!

It really is a great list and I should **look at it before every interview!**

I'll even make it here as a todo item:

- [ ] Before every interview, have a look at this list and take a few useful and interesting questions!

Here is one question that I think is a really good one that I might want to ask always:

> - How does internal communication work?

It's quite open, which I think is really nice. They can talk a lot about it then; or not so much, if it isn't so streamlined.
